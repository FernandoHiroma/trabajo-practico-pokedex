<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Pokedex</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
</head>
<body>
 <?php
$Pokemons=array("Charmander"=>array("tipo"=>"https://vignette.wikia.nocookie.net/es.pokemon/images/c/ce/Tipo_fuego.gif/revision/latest?cb=20170114100331%22","genero"=>"macho","ataque"=>"Llamarada", "url" => "https://cdn.bulbagarden.net/upload/7/73/004Charmander.png"),
				"Pikachu"=>array("tipo"=>"https://vignette.wikia.nocookie.net/es.pokemon/images/1/1b/Tipo_el%C3%A9ctrico.gif/revision/latest?cb=20170114100155","genero"=>"hembra","ataque"=>"Electrovolt", "url" => "https://sm.ign.com/t/ign_in/gallery/a/ashs-pikac/ashs-pikachu_jeab.640.jpg"),
				"Ditto"=>array("tipo"=>"https://vignette.wikia.nocookie.net/es.pokemon/images/3/32/Tipo_normal.gif/revision/latest?cb=20170114100442","genero"=>"nulo","ataque"=>"Copiar", "url" => "https://vignette.wikia.nocookie.net/es.pokemon/images/0/03/Ditto.png/revision/latest/scale-to-width-down/350?cb=20170617010358"),
				"Blastoise"=>array("tipo"=>"https://vignette.wikia.nocookie.net/es.pokemon/images/9/94/Tipo_agua.gif/revision/latest?cb=20170114100152","genero"=>"macho","ataque"=>"Hidrobomba", "url" => "https://img00.deviantart.net/3381/i/2014/232/1/0/009_blastoise_by_pklucario-d7vy4xr.png"));
	ksort($Pokemons);

?> 


<?php
if(!(empty($_GET['whoisthatpokemon']))){
    $pokbuscado = $_GET['whoisthatpokemon'];
        if(empty($Pokemons[$pokbuscado])){ 
			echo '<div class="container section"> <h2 class="red-text">'.'Ese Pokemon no existe en esta pokedex'.'</h2></div>';}  
       else{ ?>

		<div class="container section">
		<div class="row"> 
		<div class="col s6 m3 l4 xl4"></div>
	  <div class="col s12 m6 l4 xl4">	
		<div class="card">
		  <div class="card-image">
			<img  class="materialboxed" src="<?php echo $Pokemons[$pokbuscado]['url'] ?>" height=450>
			<h5 class="card-tittle-black-text center-align"><?php echo $pokbuscado ?></h5>
		  </div>
		  <div class="card-content">
			<h5 class="center-align black-text"><?php echo 'Ataque: '.$Pokemons[$pokbuscado]['ataque'] ?></h5>
			<div class="center-align">
			<img width="80" height="30" src=<?php echo $Pokemons[$pokbuscado]['tipo'] ?>>
			</div>
		  </div>
		</div> 
		</div>
	<?php } ?>
	  </div>	
	</div>
       <?php 
    }else{
	?>
	<div class="container section">
	
		<div class="row">  
		<?php foreach($Pokemons as $nombre=>$pokemon){ ?>
	  <div class="col s6 m6 l4 xl3">	
		<div class="card">
		  <div class="card-image">
			<img  class="materialboxed" src="<?php echo $pokemon['url'] ?>" height="320">
			<h5 class="card-tittle-black-text center-align"><?php echo $nombre ?></h5>
		  </div>
		  <div class="card-content">
			<h5 class="center-align black-text"><?php echo '<br> Ataque: '.$pokemon['ataque'] ?></h5>
			<div class="center-align">
			<img width="80" height="30" src=<?php echo $pokemon['tipo'] ?>>
			</div>
		  </div>
		</div> 
		</div>
	<?php } ?>
	  </div>	
	</div>
     <?php   } 
     ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

  
<script>
   document.addEventListener('DOMContentLoaded', function() {
    M.AutoInit();
  });
</script>
</body>
</html>

