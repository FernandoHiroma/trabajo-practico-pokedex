<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pokedex</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
</head>
<body>

<div class="section container">
 <div class="row">
  <form action="./pokedexDB.php" class="col s12 center-align" method="POST">
	<div class="col s3">
	</div>
		<div class="input-field col s6">
		<input type="text" name="whoisthatpokemon" id="last_name">
		<label for="last_name">Buscar Pokemon</label>
        <input type="submit" value="Buscar" class="btn">
		</div>
  </form>
 </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script>
   document.addEventListener('DOMContentLoaded', function() {
    M.AutoInit();
  });
</script>
</body>
</html>