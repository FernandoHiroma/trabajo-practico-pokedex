<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pokedex</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
</head>
<body>
<?php
$conn = mysqli_connect("localhost","root","Log Horizon","pokedex");
if(!(empty($_POST['whoisthatpokemon'])))
	{
	$sql = "Select * from pokemons where nombre='".$_POST['whoisthatpokemon']."'";
	$result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result) == 0)
			{ 
			echo '<div class="container section"> <h2 class="red-text">'.'Ese Pokemon no existe en esta pokedex'.'</h2></div>';
			}
       else{ 
	   $rows=mysqli_fetch_assoc($result)
	   ?>
		<div class="container section">
		<div class="row"> 
		<div class="col s6 m3 l4 xl4"></div>
	  <div class="col s12 m6 l4 xl4">
		<div class="card">
		  <div class="card-image">
			<img  class="materialboxed" src="<?php echo $rows['imagen']?>" height=450>
			<h5 class="card-tittle-black-text center-align"><?php echo $_POST['whoisthatpokemon'] ?></h5>
		  </div>
		  <div class="card-content">
			<h5 class="center-align black-text"><?php echo 'Ataque: '.$rows['ataque'] ?></h5>
			<div class="center-align">
			<img width="80" height="30" src=<?php echo $rows['tipo']?>>
			</div>
			<?php
			if(!$rows['tipo2']==null){
			echo "<div class='center-align'>
			<img width='80' height='30' src='".$rows['tipo2']."'>
			</div>";
			}
			?>
		  </div>
		</div> 
		<?php } ?>
		</div>
	  </div>	
	</div>
       <?php 
    }
	else{
		$sql = "select * from pokemons";
		$result = mysqli_query($conn,$sql);
	?>
	<div class="container section">
	
		<div class="row">  
		<?php 
		while ($rows=mysqli_fetch_assoc($result))
		{ ?>
		<div class="col s6 m6 l4 xl3">	
		<div class="card">
		  <div class="card-image">
			<img  class="materialboxed" src="<?php echo $rows['imagen']?>" height=450>
			<h5 class="card-tittle-black-text center-align"><?php echo $rows['nombre']?></h5>
		  </div>
		  <div class="card-content">
			<h6 class="center-align black-text"><?php echo "Ataque: ".$rows['ataque'] ?></h6>
			<div class="center-align">
			<?php
			if(!$rows['tipo2']==null){
			echo "<img width='80' height='30' src=".$rows['tipo'].">";
			echo "<img width='80' height='30' src=".$rows['tipo2'].">";
			}
			if($rows['tipo2']==null){
			echo "<img width='80' height='30' src=".$rows['tipo'].">";
			}
			?>
			</div>
		  </div>
		</div> 
		</div>
		<?php 
		} 
	}?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script>
   document.addEventListener('DOMContentLoaded', function() {
    M.AutoInit();
  });
</script>
</body>
</html>

