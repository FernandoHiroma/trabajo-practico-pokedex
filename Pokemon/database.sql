create schema pokemons_hiroma_fernando;

use pokemons_hiroma_fernando;

create table pokemons(
	nombre varchar(20) NOT NULL,
    imagen varchar(2083),
    tipo varchar(2083),
    tipo2 varchar(2083),
    ataque varchar(20)
);

insert into pokemons values
('Bulbasaur','https://cdn.bulbagarden.net/upload/thumb/2/21/001Bulbasaur.png/375px-001Bulbasaur.png','https://vignette.wikia.nocookie.net/es.pokemon/images/d/d6/Tipo_planta.gif/revision/latest?cb=20170114100444','https://vignette.wikia.nocookie.net/es.pokemon/images/1/10/Tipo_veneno.gif/revision/latest?cb=20170114100535','Latigo Cepa'),
('Charmander','https://cdn.bulbagarden.net/upload/thumb/7/73/004Charmander.png/375px-004Charmander.png','https://vignette.wikia.nocookie.net/es.pokemon/images/c/ce/Tipo_fuego.gif/revision/latest?cb=20170114100331',null,'Lanzallamas'),
('Squirtle','https://cdn.bulbagarden.net/upload/thumb/3/39/007Squirtle.png/375px-007Squirtle.png','https://vignette.wikia.nocookie.net/es.pokemon/images/9/94/Tipo_agua.gif/revision/latest?cb=20170114100152',null,'Pistola Agua'),
('Pikachu','https://vignette.wikia.nocookie.net/es.pokemon/images/7/77/Pikachu.png/revision/latest/scale-to-width-down/350?cb=20150621181250','https://vignette.wikia.nocookie.net/es.pokemon/images/1/1b/Tipo_el%C3%A9ctrico.gif/revision/latest?cb=20170114100155',null,'Impactrueno'),
('Ditto','https://vignette.wikia.nocookie.net/es.pokemon/images/0/03/Ditto.png/revision/latest/scale-to-width-down/350?cb=20170617010358','https://vignette.wikia.nocookie.net/es.pokemon/images/3/32/Tipo_normal.gif/revision/latest?cb=20170114100442',null,'Transformar'),
('Seel','https://vignette.wikia.nocookie.net/es.pokemon/images/f/f1/Seel.png/revision/latest?cb=20080909114908','https://vignette.wikia.nocookie.net/es.pokemon/images/9/94/Tipo_agua.gif/revision/latest?cb=20170114100152',null,'Rayo Hielo'),
('Ponyta','https://vignette.wikia.nocookie.net/es.pokemon/images/5/5f/Ponyta.png/revision/latest?cb=20080909114620','https://vignette.wikia.nocookie.net/es.pokemon/images/c/ce/Tipo_fuego.gif/revision/latest?cb=20170114100331',null,'Ataque Rápido'),
('Ninetales','https://vignette.wikia.nocookie.net/es.pokemon/images/9/98/Ninetales.png/revision/latest?cb=20080909113835','https://vignette.wikia.nocookie.net/es.pokemon/images/c/ce/Tipo_fuego.gif/revision/latest?cb=20170114100331',null,'Ascuas'),
('Psyduck','https://vignette.wikia.nocookie.net/es.pokemon/images/3/32/Psyduck.png/revision/latest?cb=20080909114656','https://vignette.wikia.nocookie.net/es.pokemon/images/9/94/Tipo_agua.gif/revision/latest?cb=20170114100152','https://vignette.wikia.nocookie.net/es.pokemon/images/1/15/Tipo_ps%C3%ADquico.gif/revision/latest?cb=20170114100445','Psíquico'),
('Machoke','https://vignette.wikia.nocookie.net/es.pokemon/images/c/ca/Machoke.png/revision/latest?cb=20080909112710','https://vignette.wikia.nocookie.net/es.pokemon/images/b/b7/Tipo_lucha.gif/revision/latest?cb=20170114100336',null,'Golpe Kárate'),
('Slowpoke','https://cdn.bulbagarden.net/upload/thumb/7/70/079Slowpoke.png/375px-079Slowpoke.png','https://vignette.wikia.nocookie.net/es.pokemon/images/9/94/Tipo_agua.gif/revision/latest?cb=20170114100152','https://vignette.wikia.nocookie.net/es.pokemon/images/1/15/Tipo_ps%C3%ADquico.gif/revision/latest?cb=20170114100445','Confusión'),
('Snorlax','https://vignette.wikia.nocookie.net/es.pokemon/images/0/0b/Snorlax.png/revision/latest/scale-to-width-down/350?cb=20160904204605','https://vignette.wikia.nocookie.net/es.pokemon/images/3/32/Tipo_normal.gif/revision/latest?cb=20170114100442',null,'Descansar'),
('Hitmonlee','https://vignette.wikia.nocookie.net/es.pokemon/images/0/0f/Hitmonlee.png/revision/latest?cb=20080909112242','https://vignette.wikia.nocookie.net/es.pokemon/images/b/b7/Tipo_lucha.gif/revision/latest?cb=20170114100336',null,'Patada Alta');

create table usuario(
username varchar(20) NOT NULL,
contraseña varchar(20) NOT NULL
);

insert into usuario values
("user1","1234");